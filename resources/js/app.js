
import './bootstrap';

import('preline');

async function init () {
    const node = document.querySelector("#type-text")

    while (true) {
        await node.type('PHEO.DEV')
        await sleep(2000)
        await node.delete('PHEO.DEV')
        await node.type('DEVELOP')
        await sleep(2000)
        await node.delete('DEVELOP')
        await node.type('LEARN')
        await sleep(2000)
        await node.delete('LEARN')
        await node.type('LOVE')
        await sleep(2000)
        await node.delete('LOVE')
        await node.type('STRIVE')
        await sleep(2000)
        await node.delete('STRIVE')
        await node.type('ACHIEVE')
        await sleep(2000)
        await node.delete('ACHIEVE')
    }
}


// Source code 🚩

const sleep = time => new Promise(resolve => setTimeout(resolve, time))

class TypeAsync extends HTMLHeadingElement {
    get typeInterval () {
        const randomMs = 100 * Math.random()
        return randomMs < 50 ? 10 : randomMs
    }

    async type (text) {
        for (let character of text) {
            this.innerText += character
            await sleep(this.typeInterval)
        }
    }

    async delete (text) {
        for (let character of text) {
            this.innerText = this.innerText.slice(0, this.innerText.length -1)
            await sleep(this.typeInterval)
        }
    }
}

customElements.define('type-async', TypeAsync, { extends: 'h1' })


init()
