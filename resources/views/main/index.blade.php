<!DOCTYPE html>
<html lang="ru" class="h-screen">

<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    @vite('resources/js/app.js')
    @vite('resources/css/app.css')

    <title>PHEO.DEV</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <style>
        * {
            font-family: 'Montserrat', sans-serif;
        }

        .notranslate {
            display: none
        }

        canvas {
            position: absolute
        }

        #LOGO {
            filter: url(#distortionFilter);
        }
    </style>

</head>

<body id="screen"
    class="flex flex-col justify-between w-full h-full text-2xl lg:text-4xl p-4 font-medium mx-auto select-none">

    <div class="flex flex-col lg:flex-row justify-between grow-0">
        <div class="flex flex-col items-center lg:items-start mb-2 lg:mb-0 font-bold lg:font-semibold tracking-wider">
            <h2 class="border-4 w-min border-black p-1" style="margin-bottom: -4px;">BACKEND</h2>
            <h2 class="border-4 w-min border-black p-1">DEVELOPMENT</h2>
        </div>
        <div class="text-right">
            <a class="flex flex-row lg:flex-col justify-center" href="https://vk.com/id312371183" target="_blank">
                <h2 class="lg:hover:overline block mr-1 lg:mr-0">Vladimir</h2>
                <h2 class="lg:hover:underline block">Kuznecsov</h2>
            </a>
        </div>
    </div>

    <div class="flex items-center justify-center w-full h-0 text-center grow aspect-square">
        <div class="bg-black p-3 text-5xl lg:text-7xl shadow-md shadow-black m-3 rounded-3xl tracking-wide">
            <h1 is="type-async" id="type-text" class="text-white"></h1>
        </div>
    </div>

    <div class="flex flex-row justify-evenly grow-0">
        <a class="hover:bg-zinc-200 border-4 border-b-8 border-r-8 font-semibold p-3 rounded-3xl" target="_blank"
            href="https://vk.com/id312371183">VK</a>
        <a class="hover:bg-zinc-200 border-4 border-b-8 border-r-8 font-semibold p-3 rounded-3xl" target="_blank"
            href="https://t.me/Pheo_Dev">TG</a>
        <a class="hover:bg-zinc-200 border-4 border-b-8 border-r-8 font-semibold p-3 rounded-3xl" target="_blank"
            href="https://github.com/vladimir-kuzn">GITHUB</a>
    </div>
</body>

</html>
